# Shop Services

Services in python using django and dajngo REST framework.

## Installation

Setup project
```bash
pip install pipenv
git clone https://gitlab.com/astrear/simpleshop
cd simpleshop
```

Install dependencies
```bash
pipenv install
pipenv shell
```

Setup database
```bash
cd shop
python manage.py migrate
```

Run server
```bash
python manage.py runserver
```
Endpoints documentation: (``http://{{server}}:{{port}}/docs/``).

## License
[MIT](https://choosealicense.com/licenses/mit/)