from rest_framework.generics import RetrieveUpdateDestroyAPIView, CreateAPIView, ListAPIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from orders.models import Order
from items.models import Item
from orders.serializers import OrderSerializer
from django.core.exceptions import ObjectDoesNotExist

import logging
logger = logging.getLogger(__name__)


class OrderCreateAPIView(CreateAPIView):
    authentication_classes = ((JSONWebTokenAuthentication,))
    permission_classes = (IsAuthenticated,)

    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def perform_create(self, serializer):
        item_id = self.kwargs['item_id']
        item = Item.objects.get(id=item_id)
        serializer.save(
            user=self.request.user,
            item=item
        )


class OrdersListAPIView(ListAPIView):
    authentication_classes = ((JSONWebTokenAuthentication,))
    permission_classes = (IsAuthenticated,)

    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class OrdersListByUserAPIView(ListAPIView):
    authentication_classes = ((JSONWebTokenAuthentication,))
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user)
    serializer_class = OrderSerializer


class OrdersRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    authentication_classes = ((JSONWebTokenAuthentication,))
    permission_classes = (IsAuthenticated,)

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
