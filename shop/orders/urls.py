from django.conf.urls import url
from orders import views

urlpatterns = [
    url(r'^orders/$',
        views.OrdersListAPIView.as_view(), name='order-list'),
    url(r'^orders/me$', views.OrdersListByUserAPIView.as_view(),
        name='order-list-by-user'),
    url(r'^orders/(?P<item_id>[0-9]+)$',
        views.OrderCreateAPIView.as_view(), name='order-create'),
    url(r'^orders/(?P<pk>[0-9]+)/$', views.OrdersRetrieveUpdateDestroyAPIView.as_view(),
        name='order-manage'),
]
