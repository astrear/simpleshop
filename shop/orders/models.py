from django.db import models
from items.models import Item
from users.models import User


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    address = models.CharField(max_length=100, blank=False, null=False)
    latitude = models.FloatField(null=False)
    longitude = models.FloatField(null=False)
    date = models.DateTimeField()

    class Meta:
        ordering = ('id',)
