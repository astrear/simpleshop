from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from users import views

urlpatterns = [
    url(r'^users/$', views.UserCreateAPIView.as_view(), name='user-create'),
    url(r'^users/(?P<pk>[0-9]+)/$',
        views.UserRetrieveUpdateAPIView.as_view(), name='user-query'),
    url(r'^users/(?P<pk>[0-9]+)/$',
        views.DestroyAPIView.as_view(), name='user-delete'),
]
