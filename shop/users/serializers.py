from rest_framework.serializers import ModelSerializer, CharField
from users.models import User


class UserSerializer(ModelSerializer):

    password = CharField(write_only=True)

    def create(self, validated_data):
        user = User(
            username=validated_data.get('username', None),
            email=validated_data.get('email', None),
        )
        user.set_password(validated_data.get('password', None))
        user.save()
        return user

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'password',
        ]
