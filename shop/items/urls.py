from django.conf.urls import url
from items import views

urlpatterns = [
    url(r'^items/$', views.ItemsListCreateAPIView.as_view(), name='item-list'),
    url(r'^items/(?P<pk>[0-9]+)/$', views.ItemsRetrieveUpdateDestroyAPIView.as_view(),
        name='item-manage'),
]
