from django.db import models
from users.models import User


class Item(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=False, null=False)
    image = models.CharField(max_length=100, blank=False, null=False)
    price = models.FloatField(null=False)
    description = models.CharField(max_length=250, blank=False, null=False)

    class Meta:
        ordering = ('id',)
